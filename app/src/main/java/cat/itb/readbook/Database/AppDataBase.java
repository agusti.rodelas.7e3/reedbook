package cat.itb.readbook.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import cat.itb.readbook.Database.Dao.BookDao;
import cat.itb.readbook.Models.Book;

@Database(entities = {Book.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

    public static AppDataBase INSTANCE;

    public abstract BookDao bookDao();

    public static AppDataBase getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, AppDataBase.class, "DataBase.db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
