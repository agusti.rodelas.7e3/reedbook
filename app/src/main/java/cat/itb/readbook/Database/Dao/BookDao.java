package cat.itb.readbook.Database.Dao;

import androidx.room.Dao;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cat.itb.readbook.Models.Book;

@Dao
public interface BookDao {

    @Insert
    void insert(Book b);

    @Update
    void update(Book b);

    @Query("SELECT * FROM book")
    List<Book> getAllBook();
}
