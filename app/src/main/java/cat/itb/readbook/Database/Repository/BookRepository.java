package cat.itb.readbook.Database.Repository;

import java.util.List;

import cat.itb.readbook.Database.Dao.BookDao;
import cat.itb.readbook.Models.Book;

public class BookRepository {

    BookDao dao;

    public BookRepository(BookDao dao) {
        this.dao = dao;
    }

    public void insert(Book b){
        dao.insert(b);
    }

    public void update(Book b){
        dao.update(b);
    }

    public List<Book> getAll(){
        return dao.getAllBook();
    }
}
