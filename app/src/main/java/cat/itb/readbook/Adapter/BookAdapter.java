package cat.itb.readbook.Adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;

import cat.itb.readbook.Fragments.BookListFragmentDirections;
import cat.itb.readbook.Models.Book;
import cat.itb.readbook.R;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    List<Book> books;

    public BookAdapter(List<Book> books) {
        this.books = books;
    }

    class BookViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        RatingBar ratingBar;
        ImageView imageStatus, imageCover;


        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textViewNameItem);
            ratingBar = itemView.findViewById(R.id.ratingBarItem);
            imageStatus = itemView.findViewById(R.id.imageViewItemStatus);
            imageCover = itemView.findViewById(R.id.imageViewItemCover);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavDirections navDirections = BookListFragmentDirections.actionBookListFragmentToBookFragment(books.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(navDirections);
                }
            });
        }

        public void bindData(Book book){
            name.setText(book.getName());
            ratingBar.setRating(book.getRate());
            checkStatus(book.getStatus());
            imageCover.setImageBitmap(pathToBitmap(book.getCover()));
        }

        private void checkStatus(String status){
            if(status.equals("read")){
                imageStatus.setImageResource(R.drawable.ic_baseline_check_circle_24);
            }else if(status.equals("reading")){
                imageStatus.setImageResource(R.drawable.ic_baseline_play_circle_filled_24);
            }else {
                imageStatus.setImageResource(R.drawable.ic_pending_24px);
            }
        }

        private Bitmap pathToBitmap(String path){

            if(path != null){
                Log.d("entra", "hola");
                Bitmap myBitmap = BitmapFactory.decodeFile(path);
                return myBitmap;
            }

            Log.d("entra", "hola");
            return BitmapFactory.decodeResource(itemView.getResources(), R.mipmap.llibres);
        }
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        return new BookViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(books.get(position));

    }

    @Override
    public int getItemCount() {
        return books.size();
    }
}
