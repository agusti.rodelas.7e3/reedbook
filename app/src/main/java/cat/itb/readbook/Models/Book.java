package cat.itb.readbook.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "book")
public class Book  implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_book")
    private int idBook;
    @Nullable
    private String cover;
    private String name;
    private String author;
    private String status;
    private float rate;

    public Book(String name, String cover, String author, String status, float rate) {
        this.name = name;
        this.cover = cover;
        this.author = author;
        this.status = status;
        this.rate = rate;
    }

    protected Book(Parcel in) {
        idBook = in.readInt();
        name = in.readString();
        cover = in.readString();
        author = in.readString();
        status = in.readString();
        rate = in.readFloat();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idBook);
        dest.writeString(name);
        dest.writeString(cover);
        dest.writeString(author);
        dest.writeString(status);
        dest.writeFloat(rate);
    }
}
