package cat.itb.readbook.Fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cat.itb.readbook.Database.AppDataBase;
import cat.itb.readbook.Database.Dao.BookDao;
import cat.itb.readbook.Database.Repository.BookRepository;
import cat.itb.readbook.Models.Book;
import cat.itb.readbook.R;

import static android.app.Activity.RESULT_OK;

public class BookFragment extends Fragment {
    private static final int PICK_IMAGE = 1;
    private Book book;
    private EditText editTextbookName, editTextAuthor;
    private Spinner status;
    private Button buttonAdd;
    private RatingBar ratingBar;
    private ImageView imageViewStatus;
    private ImageView imageViewCover;

    AppDataBase db;
    BookDao dao;
    BookRepository bookRepository;

    private boolean update = false;
    private String pathImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.book_fragment, container, false);

        editTextbookName = v.findViewById(R.id.editTextTextBookName);
        editTextAuthor = v.findViewById(R.id.editTextAuthor);
        status = v.findViewById(R.id.spinnerStatus);
        buttonAdd = v.findViewById(R.id.buttonAdd);
        ratingBar = v.findViewById(R.id.ratingBarBook);
        imageViewStatus = v.findViewById(R.id.imageViewStatus);
        imageViewCover = v.findViewById(R.id.imageViewCover);

        db = AppDataBase.getInstance(this.getContext());
        dao = db.bookDao();
        bookRepository = new BookRepository(dao);

        if(getArguments().getParcelable("Book") != null) {
            book = getArguments().getParcelable("Book");
            buttonAdd.setText("Update");
            update = true;

            List<String> statusList = Arrays.asList(getResources().getStringArray(R.array.status));

            editTextbookName.setText(book.getName());
            editTextAuthor.setText(book.getAuthor());
            status.setSelection(statusList.indexOf(book.getStatus()));
            ratingBar.setRating(book.getRate());
            checkStatus();
            pathImage = book.getCover();
            if(pathToBitmap(book.getCover()) != null) {
                imageViewCover.setImageBitmap(pathToBitmap(book.getCover()));
            }
        }


        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(status.getSelectedItem().toString().equals("read")){
                    ratingBar.setVisibility(View.VISIBLE);
                }else {
                    ratingBar.setVisibility(View.INVISIBLE);
                }
                checkStatus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imageViewCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(gallery, PICK_IMAGE);
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!thingsIsEmpty()) {
                    if (update) {
                        Book b = (Book) getArguments().getParcelable("Book");
                        if (ratingBar.getVisibility() == View.VISIBLE) {
                            b.setAuthor(editTextAuthor.getText().toString());
                            b.setName(editTextbookName.getText().toString());
                            b.setCover(pathImage);
                            b.setStatus(status.getSelectedItem().toString());
                            b.setRate(ratingBar.getRating());
                        } else {
                            b.setAuthor(editTextAuthor.getText().toString());
                            b.setName(editTextbookName.getText().toString());
                            b.setCover(pathImage);
                            b.setStatus(status.getSelectedItem().toString());
                            b.setRate(0);
                        }
                        bookRepository.update(b);
                        NavDirections navDirections = BookFragmentDirections.actionBookFragmentToBookListFragment();
                        Navigation.findNavController(v).navigate(navDirections);

                    } else {
                        if (ratingBar.getVisibility() == View.VISIBLE) {
                            bookRepository.insert(new Book(editTextbookName.getText().toString(),
                                    pathImage,
                                    editTextAuthor.getText().toString(),
                                    status.getSelectedItem().toString(),
                                    ratingBar.getRating()));
                        } else {
                            bookRepository.insert(new Book(editTextbookName.getText().toString(),
                                    pathImage,
                                    editTextAuthor.getText().toString(),
                                    status.getSelectedItem().toString(),
                                    0));
                        }
                        NavDirections navDirections = BookFragmentDirections.actionBookFragmentToBookListFragment();
                        Navigation.findNavController(v).navigate(navDirections);
                    }
                }else {
                    Toast.makeText(getContext(), "Campos vacios", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    public void checkStatus(){
        if(status.getSelectedItem().toString().equals("read")){
            imageViewStatus.setImageResource(R.drawable.ic_baseline_check_circle_24);
        }else if(status.getSelectedItem().toString().equals("reading")){
            imageViewStatus.setImageResource(R.drawable.ic_baseline_play_circle_filled_24);
        }else {
            imageViewStatus.setImageResource(R.drawable.ic_pending_24px);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            pathImage = getRealPathFromURI(imageUri);
            try {
                InputStream imageStream = getContext().getContentResolver().openInputStream(imageUri);
                imageViewCover.setImageBitmap(BitmapFactory.decodeStream(imageStream));
            }catch (Exception e){}
        }
        }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private Bitmap pathToBitmap(String path){
        if(path != null){
            Log.d("entra", "hola");
            Bitmap myBitmap = BitmapFactory.decodeFile(path);
            return myBitmap;
        }
        return null;
    }

    private boolean thingsIsEmpty(){
        if(editTextbookName.getText().toString().isEmpty() && editTextAuthor.getText().toString().isEmpty()){
            return true;
        }
        return false;
    }

}
