package cat.itb.readbook.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import cat.itb.readbook.Adapter.BookAdapter;
import cat.itb.readbook.Database.AppDataBase;
import cat.itb.readbook.Database.Dao.BookDao;
import cat.itb.readbook.Database.Repository.BookRepository;
import cat.itb.readbook.R;

public class BookListFragment extends Fragment {
    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    RecyclerView recyclerView;
    FloatingActionButton fButton;

    AppDataBase db;
    BookDao dao;
    BookRepository bookRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = AppDataBase.getInstance(this.getContext());
        dao = db.bookDao();
        bookRepository = new BookRepository(dao);

        checkPermissions();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vh = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        recyclerView = vh.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setAdapter(new BookAdapter(bookRepository.getAll()));

        fButton = vh.findViewById(R.id.floatingActionButton);

        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = BookListFragmentDirections.actionBookListFragmentToBookFragment(null);
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

        return vh;
    }

    private void checkPermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    getActivity(),
                    PERMISSIONS_STORAGE,
                    MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE
            );
        }
    }


}
